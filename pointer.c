include<stdio.h>
swap(int *x, int*y);
int main()
{
int a,b;
printf("enter two numbers\n");
scanf("%d%d",&a, &b);
printf("before swapping:-a=%d and b=%d\n",a,b);
swap(&a,&b);
printf("after swapping:-a=%d and b=%d\n",a,b);
return 0;
}
int swap(int *x, int *y) 
{
int temp;
temp=*x;
*x=*y;
*y=temp;
}